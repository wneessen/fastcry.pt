# fastcry.pt - A quick and easy encrypted note system

fastcry.pt has moved to [GitHub](https://github.com/wneessen/fastcry.pt). Please use that repo instead.